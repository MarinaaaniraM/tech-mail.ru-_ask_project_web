import MySQLdb
db = MySQLdb.connect(host= "localhost",
                    user="root",
                    passwd="298146576",
                    db="ask_db",
                    use_unicode=True)

c = db.cursor()

###############################################################################
# c.execute('drop database ask_db;')
# c.execute('create database ask_db character set utf8;')
# c.execute('use ask_db;')

###############################################################################
c.execute('select * from ask_userprofile \
          into outfile \'/tmp/userprofile_fake\' \
          fields terminated by \' \' \
          enclosed by \'"\' \
          lines terminated by \'\n\' ')
c.execute('select * from ask_question \
          into outfile \'/tmp/question_fake\' \
          fields terminated by \' \' \
          enclosed by \'"\' \
          lines terminated by \'\n\' ')
c.execute('select * from ask_answer \
          into outfile \'/tmp/answer_fake\' \
          fields terminated by \' \' \
          enclosed by \'"\' \
          lines terminated by \'\n\' ')

###############################################################################
# c.execute('load data infile \'/tmp/userprofile_fake\' \
#           into table ask_userprofile  \
#           fields terminated by \' \' \
#           enclosed by \'"\' \
#           lines terminated by \'\n\'')
# c.execute('load data infile \'/tmp/question_fake\' \
#           into table ask_question  \
#           fields terminated by \' \' \
#           enclosed by \'"\' \
#           lines terminated by \'\n\'')
# c.execute('load data infile \'/tmp/answer_fake\' \
#           into table ask_answer  \
#           fields terminated by \' \' \
#           enclosed by \'"\' \
#           lines terminated by \'\n\'')