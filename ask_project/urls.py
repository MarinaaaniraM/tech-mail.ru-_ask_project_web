from django.conf.urls import patterns, include, url
from django.contrib import admin
from ask.views import *
from loginsys.views import *

admin.autodiscover()

urlpatterns = patterns('',

    # =================================================================================================================
    # Pages
    url(r'^$', index),
    url(r'^new/$', index_new),
    url(r'^new_ask/$', new_ask),
    url(r'^ask/([0-9]+)?/$', ask),
    url(r'^profile/([0-9]+)?/$', profile),


    # =================================================================================================================
    # Pagination
    url(r'^page/(\d+)/$', index),
    url(r'^new/page/(\d+)/$', index_new),
    url(r'^ask/([0-9]+)?/page/(\d+)/$', ask),


    # =================================================================================================================
    # Authorization
    url(r'^log_in/$', log_in),
    url(r'^log_out/$', log_out),
    url(r'^register/$', register),
    url(r'^profile/([0-9]+)?/change/$', change_profile),


    # =================================================================================================================
    # Likes
    url(r'^ask/add_like/([0-9]+)?/$', add_ask_like),
    url(r'^ask/rm_like/([0-9]+)?/$', rm_ask_like),

    url(r'^ask/([0-9]+)?/answer/add_like/([0-9]+)?/$', add_answer_like),
    url(r'^ask/([0-9]+)?/answer/rm_like/([0-9]+)?/$', rm_answer_like),


    # =================================================================================================================
    # Add
    url(r'^ask/add_answer/([0-9]+)?/$', add_answer),
    url(r'^new_ask/add_ask/$', add_ask),


    # =================================================================================================================
    # Correct
    url(r'^ask/([0-9]+)?/correct/([0-9]+)?/$', correct_answer),


    # =================================================================================================================
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/home/marina/tech@mail.ru/ask_project/static/'}),

    url(r'^uploads/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/home/marina/tech@mail.ru/ask_project/uploads/'}),

    url(r'^admin/', include(admin.site.urls)),
)
