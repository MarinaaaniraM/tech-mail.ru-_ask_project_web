from faker import Faker
from faker import Factory
import  random as rand

fake = Factory.create()
f = Faker()

userProfileFile =   open("/tmp/userprofile_fake", 'w')
userAuth =   		open("/tmp/userauth_fake", 'w')
questionsFile =     open("/tmp/question_fake", 'w')
answersFile =       open("/tmp/answer_fake", 'w')
tagsFile =          open("/tmp/tags_fake", 'w')

usersNum =       30
questionsNum =   100
answersNum =     100
tagsNum =        30

n = 2
for i in range(2, usersNum):
    user_id = n
    n = n + 1

    userAuth.write('"' + str(user_id) + '"' + ' ')
    userAuth.write('"' + fake.password() + '"' + ' ')
    userAuth.write('"' + fake.date() + ' ' + fake.time() + '"' + ' ')
    userAuth.write('"' + str(0) + '"' + ' ')
    userAuth.write('"' + fake.first_name() + '"' + ' ')
    userAuth.write('"' + fake.last_name() + '"' + ' ')
    userAuth.write('"' + fake.email() + '"' + ' ')
    userAuth.write('"' + str(0) + '"' + ' ')
    userAuth.write('"' + str(0) + '"' + ' ')
    userAuth.write('"' + fake.date() + ' ' + fake.time() + '"')
    userAuth.write('\n')

    userProfileFile.write('"' + str(user_id) + '"' + ' ')
    userProfileFile.write('"' + str(user_id) + '"' + ' ')
    userProfileFile.write('"' + str(rand.randint(0, 100)) + '"' + ' ')
    userProfileFile.write('"' + str('/uploads/default.png') + '"')
    userProfileFile.write('\n')

    tags_id = n
    n = n + 1
    tagsFile.write('"' + str(tags_id) + '"' + ' ')
    tagsFile.write('"' + fake.word() + '"' + ' ')
    tagsFile.write('"' + str(rand.randint(0, 30)) + '"')
    tagsFile.write('\n')

    for j in range(2, questionsNum):
        question = fake.word() + ' ' + fake.word() + ' ' + fake.word() + '?'
        question_id = n
        n = n + 1

        questionsFile.write('"' + str(question_id) + '"' + ' ')
        questionsFile.write('"' + question + '"' + ' ')
        questionsFile.write('"' + fake.text() + '"' + ' ')
        questionsFile.write('"' + str(user_id) + '"' + ' ')
        questionsFile.write('"' + fake.date() + ' ' + fake.time() + '"' + ' ')
        questionsFile.write('"' + str(rand.randint(0, 10000)) + '"')
        questionsFile.write('"' + str(tags_id) + '"')
        questionsFile.write('\n')

        for k in range(2, answersNum):
            answer_id = n
            n = n + 1
            answersFile.write('"' + str(answer_id) + '"' + ' ')
            answersFile.write('"' + str(question_id) + '"' + ' ')
            answersFile.write('"' + fake.text() + '"' + ' ')
            answersFile.write('"' + str(user_id) + '"' + ' ')
            answersFile.write('"' + fake.date() + ' ' + fake.time() + '"' + ' ')
            answersFile.write('"' + str(rand.randint(0, 10000)) + '"')
            answersFile.write('\n')

















