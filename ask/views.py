from django.shortcuts import render_to_response, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.core.context_processors import csrf
from django.core.paginator import Paginator
from form import *


# =====================================================================================================================
def index(request, page_number=1):
    args = {}
    args.update(csrf(request))
    args['questions'] = Paginator(Ask.objects.all().order_by('-rating'), 20).page(page_number)
    args['username'] = auth.get_user(request).username
    args['is_new_sort'] = False
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    return render_to_response('index.html', args)

def index_new(request, page_number=1):
    args = {}
    args.update(csrf(request))
    args['questions'] = Paginator(Ask.objects.all().order_by('-create_date'), 20).page(page_number)
    args['username'] = auth.get_user(request).username
    args['is_new_sort'] = True
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    return render_to_response('index.html', args)

def new_ask(request):
    args = {}
    args.update(csrf(request))
    args['form'] = NewAskForm
    args['username'] = auth.get_user(request).username
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    return render_to_response('new_ask.html', args)

def ask(request, ask_id, page_number=1):
    args = {}
    args.update(csrf(request))
    args['question'] = Ask.objects.get(id=ask_id)
    args['answers'] = Paginator(Answer.objects.filter(question_id=ask_id).order_by('-create_date'), 10).page(page_number)
    args['form'] = AnswerForm
    args['valid_user'] = False
    args['username'] = auth.get_user(request).username
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
        if Ask.objects.get(id=ask_id).user == UserProfile.objects.get(user=auth.get_user(request)):
            args['valid_user'] = True
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    return render_to_response('ask.html', args)

def profile(request, user_id):
    args = {}
    args.update(csrf(request))
    args['user_base'] = auth.get_user(request)
    args['user_other'] = UserProfile.objects.get(id=user_id)

    args['username'] = auth.get_user(request).username
    args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    return render_to_response('profile.html', args)


# =====================================================================================================================
def add_ask_like(request, ask_id):
    try:
        like = LikeAsk.objects.get(user=auth.get_user(request), ask=ask_id)
        if like.is_like:
            return redirect('/')
        else:
            quest = Ask.objects.get(id=ask_id)
            user = UserProfile.objects.get(user=quest.user)
            LikeAsk.objects.filter(user=UserProfile.objects.get(user=auth.get_user(request)),
                                   ask=Ask.objects.get(id=ask_id)).delete()
            quest.rating += 1
            user.rating += 1
            user.save()
            quest.save()
            return redirect('/')
    except:
        quest = Ask.objects.get(id=ask_id)
        user = UserProfile.objects.get(user=quest.user)
        LikeAsk(user=UserProfile.objects.get(user=auth.get_user(request)),
                ask=Ask.objects.get(id=ask_id), is_like=True).save()
        quest.rating += 1
        user.rating += 1
        user.save()
        quest.save()
        return redirect('/')


def rm_ask_like(request, ask_id):
    try:
        like = LikeAsk.objects.get(user=auth.get_user(request), ask=ask_id)
        if not like.is_like:
            return redirect('/')
        else:
            quest = Ask.objects.get(id=ask_id)
            user = UserProfile.objects.get(user=quest.user)
            LikeAsk.objects.get(user=UserProfile.objects.get(user=auth.get_user(request)),
                                ask=Ask.objects.get(id=ask_id)).delete()
            quest.rating -= 1
            user.rating -= 1
            user.save()
            quest.save()
            return redirect('/')
    except:
        quest = Ask.objects.get(id=ask_id)
        user = UserProfile.objects.get(user=quest.user)
        LikeAsk(user=UserProfile.objects.get(user=auth.get_user(request)),
                ask=Ask.objects.get(id=ask_id), is_like=False).save()
        quest.rating -= 1
        user.rating -= 1
        user.save()
        quest.save()
        return redirect('/')


# =====================================================================================================================
def add_answer_like(request, ask_id, answer_id):
    try:
        like = LikeAnswer.objects.get(user=auth.get_user(request), answer=answer_id)
        if like.is_like:
            return redirect('/ask/%s/' % ask_id)
        else:
            answer = Answer.objects.get(id=answer_id)
            user = UserProfile.objects.get(user=answer.user)
            LikeAnswer.objects.filter(user=UserProfile.objects.get(user=auth.get_user(request)),
                                      answer=Answer.objects.get(id=answer_id)).delete()
            answer.rating += 1
            user.rating += 1
            user.save()
            answer.save()
            return redirect('/ask/%s/' % ask_id)
    except:
        answer = Answer.objects.get(id=answer_id)
        user = UserProfile.objects.get(user=answer.user)
        LikeAnswer(user=UserProfile.objects.get(user=auth.get_user(request)),
                   answer=Answer.objects.get(id=answer_id), is_like=True).save()
        answer.rating += 1
        user.rating += 1
        user.save()
        answer.save()
        return redirect('/ask/%s/' % ask_id)


def rm_answer_like(request, ask_id, answer_id):
    try:
        like = LikeAnswer.objects.get(user=auth.get_user(request), answer=answer_id)
        if not like.is_like:
            return redirect('/ask/%s/' % ask_id)
        else:
            answer = Answer.objects.get(id=answer_id)
            user = UserProfile.objects.get(user=answer.user)
            LikeAnswer.objects.get(user=UserProfile.objects.get(user=auth.get_user(request)),
                                   answer=Answer.objects.get(id=answer_id)).delete()
            answer.rating -= 1
            user.rating -= 1
            user.save()
            answer.save()
            return redirect('/ask/%s/' % ask_id)
    except:
        answer = Answer.objects.get(id=answer_id)
        user = UserProfile.objects.get(user=answer.user)
        LikeAnswer(user=UserProfile.objects.get(user=auth.get_user(request)),
                   answer=Answer.objects.get(id=answer_id), is_like=False).save()
        answer.rating -= 1
        user.rating -= 1
        user.save()
        answer.save()
        return redirect('/ask/%s/' % ask_id)


# =====================================================================================================================
def add_answer(request, ask_id):
    if request.POST:
        form = AnswerForm(request.POST)
        if form.is_valid():
            answ = form.save(commit=False)
            answ.question = Ask.objects.get(id=ask_id)
            answ.user = UserProfile.objects.get(user=auth.get_user(request))
            form.save()
            description = request.POST.get('description', '')
            ask_user = User.objects.get(id=UserProfile.objects.get(ask=ask_id).id)
        return redirect('/ask/%s/' % ask_id)

def add_ask(request):
    args = {}
    args.update(csrf(request))
    if request.POST:
        form = NewAskForm(request.POST)
        if form.is_valid():
            new_ask = form.save(commit=False)
            new_ask.user = UserProfile.objects.get(user=auth.get_user(request))
            tags_array = request.POST.get('name', '')
            form.save()
            for tag in tags_array.split(', '):
                if tag != '':
                    try:
                        tag = Tag.objects.get(name=tag)
                        tag.count += 1
                        tag.save()
                        new_ask.tags.add(tag)
                    except:
                        t = Tag.objects.create(name=tag, count=1)
                        t.save()
                        new_ask.tags.add(Tag.objects.get(name=tag))
            return redirect('/new/')
        else:
            args['add_ask_error'] = 'Please, enter the correct data.'
            return render_to_response('new_ask.html', args)


# =====================================================================================================================
def correct_answer(request, ask_id, answer_id):
    try:
        answer = Answer.objects.get(id=answer_id)
        if answer.correct:
            answer.correct = False
        else:
            answer.correct = True
        answer.save()
    except ObjectDoesNotExist:
        raise Http404
    return redirect('/ask/%s/' % ask_id)