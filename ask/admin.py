from django.contrib import admin
from ask.models import *

admin.site.register(UserProfile)
admin.site.register(Ask)
admin.site.register(Answer)
admin.site.register(Tag)
admin.site.register(LikeAsk)
admin.site.register(LikeAnswer)
