# -*- coding:utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Tag')
    count = models.PositiveIntegerField(default=0)
    class Meta:
        verbose_name_plural = 'Tag'
    def __unicode__(self):
        return self.name

class UserProfile (models.Model):
    user = models.ForeignKey(User, unique=True)
    rating = models.IntegerField(verbose_name='Rating', default=0)
    avatar = models.ImageField(upload_to='avatars', verbose_name='Foto', default='/uploads/default.png')
    def __unicode__(self):
        return self.user.username

class Ask(models.Model):
    title = models.CharField(max_length=100, verbose_name='Title')
    description = models.TextField(verbose_name='Description')
    user = models.ForeignKey(UserProfile)
    create_date = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(verbose_name='Rating', default=0)
    tags = models.ManyToManyField(Tag, blank=True)

    class Meta:
        verbose_name = 'questions'
        verbose_name_plural = 'Question'
    def __unicode__(self):
        return self.title

class Answer(models.Model):
    question = models.ForeignKey(Ask)
    description = models.TextField(verbose_name='Answer')
    user = models.ForeignKey(UserProfile)
    create_date = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(verbose_name='Rating', default=0)
    correct = models.BooleanField(verbose_name='Correct', default=False)
    class Meta:
        verbose_name = 'answers'
        verbose_name_plural = 'Answer'
    def __unicode__(self):
        return self.description

class LikeAsk(models.Model):
    user = models.ForeignKey(UserProfile)
    ask = models.ForeignKey(Ask)
    is_like = models.BooleanField(verbose_name='Is like?')
    class Meta:
        verbose_name = 'likes_asks'
        verbose_name_plural = 'Like_ask'
    def __unicode__(self):
        return self.user

class LikeAnswer(models.Model):
    user = models.ForeignKey(UserProfile)
    answer = models.ForeignKey(Answer)
    is_like = models.BooleanField(verbose_name='Is like?')
    class Meta:
        verbose_name = 'likes_answers'
        verbose_name_plural = 'Like_answer'
    def __unicode__(self):
        return self.user
