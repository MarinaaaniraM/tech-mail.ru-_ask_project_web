# -*- coding:utf-8 -*-

from django.forms import ModelForm
from django.contrib import auth
from models import *

class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['description']

class NewAskForm(ModelForm):
    class Meta:
        model = Ask
        fields = ['title', 'description', 'tags']
