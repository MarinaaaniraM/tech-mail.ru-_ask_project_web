# -*- coding:utf-8 -*-

from django.forms import ModelForm
from ask.models import *

class BaseProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'email']

class OtherProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ['avatar']
