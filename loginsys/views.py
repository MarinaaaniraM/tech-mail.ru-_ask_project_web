from django.shortcuts import render_to_response, redirect
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth
from form import *

def log_in(request):
    args = {}
    args.update(csrf(request))
    args['username'] = auth.get_user(request).username
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            args['login_error'] = 'User not found.'
            return render_to_response('log_in.html', args)
    return render_to_response('log_in.html', args)

def log_out(request):
    auth.logout(request)
    return redirect("/")

def register(request):
    args = {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    args['username'] = auth.get_user(request).username
    if args['username']:
        args['user_id'] = UserProfile.objects.get(user=auth.get_user(request)).id
    args['best_users'] = UserProfile.objects.order_by('-rating')[0:10]
    args['tags'] = Tag.objects.all()
    if request.POST:
        new_user_form = UserCreationForm(request.POST)
        if new_user_form.is_valid():
            new_user_form.save()
            new_user = auth.authenticate(username=new_user_form.cleaned_data['username'],
                                         password=new_user_form.cleaned_data['password2'])
            user = UserProfile(user=new_user)
            user.save()
            auth.login(request, new_user)
            return redirect('/')
        else:
            args['register_error'] = 'Please, enter the correct data.'
            return render_to_response('register.html', args)
    return render_to_response('register.html', args)

def change_profile(request, user_id):
    args = {}
    args.update(csrf(request))
    user_base = auth.get_user(request)
    user_other = UserProfile.objects.get(user=user_base)
    if request.POST:
        form = BaseProfileForm(request.POST)
        other_form = OtherProfileForm(request.POST, request.FILES)
        if form.is_valid() and other_form.is_valid():

            user_base.email = request.POST.get('email', '')
            user_base.first_name = request.POST.get('first_name', '')
            user_base.save()

            user_other.avatar = request.POST.get('avatar', '')
            user_other.save()
            return redirect('/profile/%s/' % user_id, args)
    return redirect('/profile/%s/' % user_id, args)